Installation du Serveur/Broker RabbitMQ sur Windows
---------------------------------------------------

1- Télécharger RabbitMQ (https://www.rabbitmq.com/install-windows.html#installer) et la version de la VM Erlang (https://www.rabbitmq.com/which-erlang.html) 
    correspondant et les installer. Erlang est installé en premier. 
    
2- Créer en ligne de commande un nouveau user qui sera utilisé dans les applications pour l'envoi et la reception des messages :
      (==> NB : L'installation de RabbitMQ crée un user par défaut de type administrateur de login/pwd = guest/guest)
      Ouvrir la console contenue dans le dossier 'RabbitMQ Server' créé à la suite de l'installation dans le menu démarrer de Windows et saisir la commande :
      
              rabbitmqctl.bat add_user test test1                        (on crée ainsi un user où login/pwd = test/test1)
              
3- Ajout du user test/test1 au vhost / Rabbitmq : 
      (==> NB : Rabbitmq fonctionne en terme de gestion des connections à ses ressources, sur la base d'un découpage logique basé sur les vhost.
        Un vhost est donc une sorte de partition logique dans laquelle ont retrouve un ensemble de ressources Rabbitmq (Exchange - Binding - Queues) nécessaires
          au fonctionnement d'un ou plusieurs broker. Chaque vhost est par conséquent indépendant des autres et dispose d'un nom nom unique.
        Le vshost par défaut créer par Rabbitmq à l'installation est nommé "/"
      )
      
           rabbitmqctl.bat set_permissions -p test / .* .* .* .*             (on donne ainsi la permission au user test/test1 de pouvoir lire et écrire dans les queues du vhost /)

4- Dans le dossier Rabbitmq du menu Démarrer de Windows, lancer l'exécution de Rabbitmq sur votre machine en cliquant sur le bouton 'RabbitMQ Service - start'

        (==> NB : si vous rencontrez un problème de type "%compsec%" lors de ce lancement, alors éditer le bouton 'RabbitMQ Service - start' : clic droit > propriétés, 
             et remplacer le chemin ${PATH}/%compsec% par celui qui pointe vers votre cmd, i.e : C:\Windows\System32\cmd.exe)

5- Une fois que Rabbitmq est démarré, activer sa console web d'administration graphique afin de pousuivre la suite du paramétrage plus facilement :
		Toujours dans la console Rabbitmq du dossier 'RabbitMQ Server' du menu Démarrer de Windows, saisir la commande :
		     
		     rabbitmq-plugins enable rabbitmq_management
		     
6- Accéder à la page web d'administration de Rabbitmq sur son url par défaut et se connecter avec le user guest/guest : http://localhost:15672/

7- Dans cette page d'administration, nous allons dans l'onglet Queue pour créer (très aisément) 2 queues : 'emarket.order.queue' et 'emarket.order.bill.queue'

8- Dans cette page d'administration, nous allons ensuite dans l'onglet Exchange pour créer 2 Routing key : 'emarket.order.rk1' et 'emarket.order.rk2' afin de réaliser
      un binding sur l'Exchange 'amq.direct' suivant la règle :
      
         amq.direct ---> emarket.order.rk1 ---> emarket.order.queue
         amq.direct ---> emarket.order.rk2 ---> emarket.order.bill.queue
         
         ==> La stratégie de communication Rabbitmq utilisée dans notre appplication est donc la 'Direct Exchange' qui consiste à router tout message :
            - qui arrive dans la queue 'emarket.order.queue' ssi Exchange = 'amq.direct' et routing key = 'emarket.order.rk1'
            - qui arrive dans la queue 'emarket.order.bill.queue' ssi Exchange = 'amq.direct' et routing key = 'emarket.order.rk2'
         


==> les 8 étapes ci-dessus offrent donc une configuration minimale de prod permettant d'utiliser un Broker Rabbitmq en stratégie 'Direct Exchange' 
    avec un user qui n'est pas celui par défaut (guest - qui ne fonctionne qu'en local)               
