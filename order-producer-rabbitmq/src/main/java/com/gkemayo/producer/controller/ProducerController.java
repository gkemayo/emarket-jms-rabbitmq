package com.gkemayo.producer.controller;

import java.time.LocalDateTime;

import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gkemayo.producer.model.Order;


@RestController
@RequestMapping("/producer")
@ConfigurationProperties
public class ProducerController {
	
	private static final String KO = "KO - ";

	private static final String OK = "OK";

	private static final String ORDER_SENDING_DATE = "ORDER_SENDING_DATE";

	@Autowired
	private RabbitTemplate rabbitTemplate;
	
	@Autowired
	private ObjectMapper mapper;
	
	@Value("${order.routing.key}")
	private String orderRoutingKey;
	
	@Value("${order.bill.routing.key}")
	private String orderBillRoutingKey;
	
	@Value("${direct.exchange}")
	private String directExchange;
	
	@PostMapping("/send/order")
	public ResponseEntity<String> submitOrder(@RequestBody Order order){
		
		try {
			
			//we serialize the LocalDateTime in String Json format to easily deserialize it in the consumer app
			String dateTimeStr = mapper.writeValueAsString(LocalDateTime.now());
			
			//send to order treatement queue
			rabbitTemplate.convertAndSend(directExchange, orderRoutingKey, order, message -> {
				MessageProperties messageProperties = message.getMessageProperties(); 
				messageProperties.setHeader(ORDER_SENDING_DATE, dateTimeStr);
				MessageConverter converter = rabbitTemplate.getMessageConverter();
				message = converter.toMessage(order, messageProperties); 
				return message;
			});
			
			//send to billing queue
			rabbitTemplate.convertAndSend(directExchange, orderBillRoutingKey, order);
			
			return new ResponseEntity<String>(OK, HttpStatus.OK);
		} catch (AmqpException | JsonProcessingException e  ) {
			return new ResponseEntity<String>(KO + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}

}
