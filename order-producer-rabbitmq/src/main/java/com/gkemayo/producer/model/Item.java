package com.gkemayo.producer.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Item implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String name;
	
	private Integer quantity;
	
	private Double price;

}
