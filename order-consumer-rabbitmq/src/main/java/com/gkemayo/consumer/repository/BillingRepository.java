package com.gkemayo.consumer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gkemayo.consumer.model.Bill;

@Repository
public interface BillingRepository extends JpaRepository<Bill, Long> {

}
