package com.gkemayo.consumer.service;

import java.time.LocalDateTime;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gkemayo.consumer.model.Bill;
import com.gkemayo.consumer.model.OrderMessage;
import com.gkemayo.consumer.repository.BillingRepository;
import com.gkemayo.consumer.repository.OrderMessageRepository;

@Service
public class ConsumerService {
	
	private static final String ORDER_SENDING_DATE = "ORDER_SENDING_DATE";

	@Autowired
	OrderMessageRepository consumerRepository;
	
	@Autowired
	BillingRepository billingRepository;
	
	@Autowired
	private ObjectMapper mapper;
	
	@RabbitListener(queues = "${order.queue}")
	public void receiveOrder(@Payload OrderMessage orderMessage,  Message message) throws JsonMappingException, JsonProcessingException, InterruptedException {
		
		MessageProperties messageProperties = message.getMessageProperties(); 
		String dateTimeJsonStr = messageProperties.getHeader(ORDER_SENDING_DATE);
		orderMessage.setSendingDate(mapper.readValue(dateTimeJsonStr, LocalDateTime.class));
		
		//simulate a long time processing orderMessage : 2 seconds
		Thread.sleep(2000);
		
		//then we archive this orderMessage in the database
		consumerRepository.save(orderMessage);
	}
	
	@RabbitListener(queues = "${order.bill.queue}")
	public void receiveOrderBill(@Payload OrderMessage orderMessage,  Message message) throws JsonMappingException, JsonProcessingException, InterruptedException {
		
		@SuppressWarnings("static-access")
		Bill bill = new Bill().builder().customerEmail(orderMessage.getCustomerEmail())
				                        .globalAmount(orderMessage.getItemList().stream()
				                        		                                .mapToDouble(item -> item.getPrice())
				                        		                                .sum()).build();
		
		//simulate a long time processing Bill : 3 seconds
		Thread.sleep(3000);
				
		//then we archive this Bill in the database
		billingRepository.save(bill);
		
		//...notify the customer that the treatment of his orderMessage has been done and send him the Bill : via an email for example.
	}

}
